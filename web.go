package main

import (
	"fmt"
	"log"
	"net/http"
	"os/exec"
)

func main() {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		out, err := exec.Command("bash", "main.sh").Output()
		if err != nil {
			log.Fatal(err)
		}
		fmt.Fprintf(w, string(out))
	})
	http.ListenAndServe(":7777", nil)
}
