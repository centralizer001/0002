package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
)

func main() {
	resp, _ := http.Get("http://0.0.0.0:7777/")
	 fmt.Println(resp)
	 body, _ := ioutil.ReadAll(resp.Body)
	 fmt.Println(string(body))
	 resp.Body.Close()
}
