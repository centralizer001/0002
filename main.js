const http = require('http');
const { exec } = require('child_process');

const hostname = '0.0.0.0';
const port = 7777;

const server = http.createServer((req, res) => {
    exec('bash main.sh', (err, stdout, stderr) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log(stdout);
    });
    res.statusCode = 200;
    res.setHeader('Content-Type', 'text/plain');
    res.end('Hello World\n');
});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});
